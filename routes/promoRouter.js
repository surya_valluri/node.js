const express = require('express');
const bodyParser = require('body-parser');

const promoRouter = express.Router();

promoRouter.use(bodyParser.json());

promoRouter.route('/')
.all((req,res,next) => {
    
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    next();
})
.get((req, res, next) => {

    res.end('will send all promotions to you');
})
.post((req, res, next) => {

    res.end('will add the promotion: ' + req.body.name + 'with details:' + req.body.description);
})
.put((req, res, next) => {

    res.statusCode = 403;
    res.end('Put is not supported');
})
.delete((req, res, next) => {

    res.end('Deleting all the promotions');
});

promoRouter.route('/:promoId')

.all((req, res, next) => {

    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    next();

})
.get((req, res, next) => {

    res.end('will send details of the promotion' + req.params.promoId + 'to you');
})

.post((req, res, next) => {

    res.statusCode = 403;
    res.end('Post is not supported');
})

.put((req, res, next) => {

    res.statusCode = 403;
    res.write('updating the dish :' + req.params.promoId);
    res.end('will update the dish : ' + req.body.name + 'with details:' + req.body.description);

})

.delete((req, res, next) => {

    res.end('Deleting the dish' + req.params.promoId);
});

module.exports = promoRouter;
